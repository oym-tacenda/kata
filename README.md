README - Jeffrey 

This is a vending machine Kata.

If you want to complie this application yourself follow the below instructions
	- navigate to this directory within cmd
	- type 'node app'
	
If you want to run the test scripts
	- navigate to this directory within cmd
	- type 'node ./res/mocha/bin/mocha test'
	

It uses nodejs, expressjs, express-sessions and renders the client pages with ejs.

One note that I wish I would have taken into account but I just learned about it today 4/27/2017 is socket.io..
        - This feature would allow for real time data transfer. Thus no need for a page refresh... It would make the UI cleaner 

I honestly feel there is a lot more that I could do with it however, I'll wait till we discuss this.

Status: 
	Features complete however there is one note to be had
		- Exact Change Only
			- I have determined that this will never happen in our environment 
			  because we are not using dollar bills, which is where that problem stems from.