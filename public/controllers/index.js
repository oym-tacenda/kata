$( document ).ready(function() {

});

function handleCoinInsert(data, textStatus){
    window.location.href=window.location.href;
}

function insertCoin(valWeight, valSize){
    $.ajax({
        type: "POST",
        url: '/coins',
        data: {weight: valWeight, size:valSize},
        success: handleCoinInsert,
        dataType: 'json'
    });
}

function requestCola(){
    window.location.href='/cola';
}

function requestChips(){
    window.location.href='/chips';
}

function requestCandy(){
    window.location.href='/candy';
}

function returnCoins(){
    window.location.href='/return';
}