var port = process.env.PORT || 3000,
    fs = require('fs'),
	path = require('path'),
	express = require('./res/express/index'),
	app = express(),
	bodyParser = require('./res/express/body-parser/index'),
	session = require('./res/express-session/index'),
	dec = require('./res/decimal');

var coins = {
	n: 10,
	d: 0,
	q: 0
}
	
app.use(session({
    secret: '2F44-4420-XepQL6a9',
    resave: true,
    saveUninitialized: true
}));

app.set('view engine', 'ejs');

//We must decalare our files so that our html can use them...
app.use(express.static(path.join(__dirname, './public')));
//support parsing of application/json type post data
app.use(bodyParser.json());
//support parsing of application/x-www-form-urlencoded post data
app.use(bodyParser.urlencoded({ extended: true }));
 
/*
 *	This is going to be the money section to accept coins...
 */
app.post('/coins', function (req, res) {
	var coin = determineCoin(req.body.weight, req.body.size);
	if(coin == STR('penny')){
		return res.json({response: 'E'});
	}else if(coin == STR('nickel')){
		if(req.session.vm){
			if(!addNickels(req, res)) {
				return res.json({response: 'E'});
			}
		}
		return res.json({response: 'S'});
	}else if(coin == STR('dime')){
		if(req.session.vm){
			if(!addDimes(req, res)) {
				return res.json({response: 'E'});
			}
		}
		return res.json({response: 'S'});
	}else if(coin == STR('quarter')){
		if(req.session.vm){
			if(!addQuarter(req, res)) {
				return res.json({response: 'E'});
			}
		}
		return res.json({response: 'S'});
	}
});

app.get('/cola', function(req, res){
	if(!req.session.vm){
		res.send("<span id=\"price\">$" + STR('COLA_PRICE') + "</span><script>setTimeout(function(){ window.location.href='/index'; }, 3000);</script>");
	}else if(req.session.vm.current_CoinValue >= req.session.vm.colaPrice){
		if(issueCola(req, res)){
			handleChangeReturn(req, res);
		}
		res.redirect('/');
	}else{
		req.session.vm.currentAction = STR('NOT_ENOUGH_FUNDS_COLA');
		res.redirect('/');
	}
});

app.get('/chips', function(req, res){
	if(!req.session.vm){
		res.send("<span id=\"price\">$0.50</span><script>setTimeout(function(){ window.location.href='/index'; }, 3000);</script>");
	}else if(req.session.vm.current_CoinValue >= req.session.vm.chipsPrice){
		if(issueChips(req, res)){
			handleChangeReturn(req, res);
		}
		res.redirect('/');
	}else{
		req.session.vm.currentAction = STR('NOT_ENOUGH_FUNDS_CHIPS');
		res.redirect('/');
	}
});

app.get('/candy', function(req, res){
	if(!req.session.vm){
		res.send("<span id=\"price\">$0.65</span><script>setTimeout(function(){ window.location.href='/index'; }, 3000);</script>");
	}else if(req.session.vm.current_CoinValue >= req.session.vm.candyPrice){
		if(issueCandy(req, res)){
			handleChangeReturn(req, res);
		}
		res.redirect('/');
	}else{
		req.session.vm.currentAction = STR('NOT_ENOUGH_FUNDS_CANDY');
		res.redirect('/');
	}
});

app.get('/return', function(req, res){
	handleChangeReturn(req, res);
	res.redirect('/');
});

app.get('/index', function(req, res){
    res.redirect('/');
});

app.get('/test/:testCase', function(req, res){
	if(!req.session.vm){
		var VendingMachine = initVM();
		req.session.vm = VendingMachine;
	}
	if(req.params.testCase == '1'){
		for(var i = 0; i<10; i++){
			if(req.session.vm.q > 0) {
				addQuarter(req, res);
			}
		}
		if(req.session.vm.current_CoinValue >= req.session.vm.colaPrice){
			if(issueCola(req, res)){
				handleChangeReturn(req, res);
			}
		}
	}else if(req.params.testCase == '2'){
		for(var i = 0; i<1; i++){
			addQuarter(req, res);
		}
		for(var i = 0; i<3; i++){
			addDimes(req, res);
		}
		for(var i = 0; i<10; i++){
			addNickels(req, res);
		}
		if(req.session.vm.current_CoinValue >= req.session.vm.colaPrice){
			if(issueCandy(req, res)){
				handleChangeReturn(req, res);
			}
		}
	}else if(req.params.testCase == '3'){
		for(var i = 0; i<10; i++){
			addQuarter(req, res);
		}
		for(var i = 0; i<10; i++){
			addDimes(req, res);
		}
		for(var i = 0; i<10; i++){
			addNickels(req, res);
		}
		handleChangeReturn(req, res);
	}
	else if(req.params.testCase == '4'){
		for(var i = 0; i<10; i++){
			addQuarter(req, res);
		}
		for(var i = 0; i<2;i++){
			if(req.session.vm.current_CoinValue >= req.session.vm.colaPrice){
				issueCola(req, res);
			}
		}
	}

	var  paramMap = {displayVal: '', reload: false};
	handleCase(req, res, paramMap);
	res.render('index', {coinCount: paramMap.displayVal, 
						shouldLoad: paramMap.reload, 
						coinAmount: {p: req.session.vm.p,
									 n: req.session.vm.n,
									 d: req.session.vm.d,
									 q: req.session.vm.q} });
});

app.get('/', function(req, res){
	if(!req.session.vm){
		var VendingMachine = initVM();
		req.session.vm = VendingMachine;
	}
	var  paramMap = {displayVal: '', reload: false};
	handleCase(req, res, paramMap);
	req.session.vm.currentAction = STR('init');
	res.render('index', {coinCount: paramMap.displayVal, 
						shouldLoad: paramMap.reload, 
						coinAmount: {p: req.session.vm.p,
									 n: req.session.vm.n,
									 d: req.session.vm.d,
									 q: req.session.vm.q} });
});

//404 ERROR
app.get('*', function(req, res){
  //Since this is a vending machine we dont respond with any error messages..	
  res.redirect('/');
});

app.listen(port);

function initVM(){
	return {
				current_CoinValue: 0,
				colaPrice:  STR('COLA_PRICE'),
				chipsPrice: STR('CHIPS_PRICE'),
				candyPrice: STR('CANDY_PRICE'),
				currentAction: STR('init'),
				p: 1,
				n: 10,
				d: 10,
				q: 10,
				inv: {
					cola: 1,
					candy: 2,
					chips: 2
				}
			};
}

function determineCoin(weight, size){
	//weight is in grams & size is in mm its the Diameter
	if(weight == 2.5 && size == 19.05){
		return STR('penny');
	}else if(weight == 5 && size == 21.21){
		return STR('nickel');
	}else if(weight == 2.268 && size == 17.91){
		return STR('dime');
	}else if(weight == 5.67 && size == 24.26){
		return STR('quarter');
	}
}

function handleChangeReturn(req, res){
	while(req.session.vm.current_CoinValue>0){
		if(coins.q > 0 && dec(req.session.vm.current_CoinValue.toString()).sub('0.25').toNumber() >= 0){
			coins.q = coins.q - 1;
			req.session.vm.q = req.session.vm.q + 1;
			req.session.vm.current_CoinValue = dec(req.session.vm.current_CoinValue.toString()).sub('0.25').toNumber()
		}else if(coins.d > 0 && dec(req.session.vm.current_CoinValue.toString()).sub('0.10').toNumber() >= 0){
			coins.d = coins.d - 1;
			req.session.vm.d = req.session.vm.d + 1;
			req.session.vm.current_CoinValue = dec(req.session.vm.current_CoinValue.toString()).sub('0.10').toNumber()
		}else if(coins.n > 0 && dec(req.session.vm.current_CoinValue.toString()).sub('0.05').toNumber() >= 0){
			coins.n = coins.n - 1;
			req.session.vm.n = req.session.vm.n + 1;
			req.session.vm.current_CoinValue = dec(req.session.vm.current_CoinValue.toString()).sub('0.05').toNumber()
		}
	}
}

function addQuarter(req, res){
	if(req.session.vm.q > 0) {
		coins.q = coins.q + 1;
		req.session.vm.q = req.session.vm.q - 1;
		req.session.vm.current_CoinValue = dec(req.session.vm.current_CoinValue.toString()).add('0.25').toNumber();
		return true;
	}else{
		return false;
	}
}

function addDimes(req, res){
	if(req.session.vm.d > 0) {
		coins.d = coins.d + 1;
		req.session.vm.d = req.session.vm.d - 1;
		req.session.vm.current_CoinValue = dec(req.session.vm.current_CoinValue.toString()).add('0.1').toNumber();
		return true;
	}else{
		return false;
	}
}

function addNickels(req, res){
	if(req.session.vm.n > 0) {
		coins.n = coins.n + 1;
		req.session.vm.n = req.session.vm.n - 1;
		req.session.vm.current_CoinValue = dec(req.session.vm.current_CoinValue.toString()).add('0.05').toNumber();
		return true;
	}else{
		return false;
	}
}

function issueCandy(req, res){
	if(req.session.vm.inv.candy > 0){
		req.session.vm.inv.candy = req.session.vm.inv.candy - 1;
		req.session.vm.current_CoinValue = dec(req.session.vm.current_CoinValue.toString()).sub(req.session.vm.candyPrice).toNumber();
		req.session.vm.currentAction = STR('candy');
		return true;
	}else{
		req.session.vm.currentAction = STR('sold_out');
		return false;
	}
}

function issueChips(req, res){
	if(req.session.vm.inv.chips > 0){
		req.session.vm.inv.chips = req.session.vm.inv.chips - 1;
		req.session.vm.current_CoinValue = dec(req.session.vm.current_CoinValue.toString()).sub(req.session.vm.chipsPrice).toNumber();
		req.session.vm.currentAction = STR('chips');
		return true;
	}else{
		req.session.vm.currentAction = STR('sold_out');
		return false;
	}
}

function issueCola(req, res){
	if(req.session.vm.inv.cola > 0){
		req.session.vm.inv.cola = req.session.vm.inv.cola - 1;
		req.session.vm.current_CoinValue = dec(req.session.vm.current_CoinValue.toString()).sub(req.session.vm.colaPrice).toNumber();
		req.session.vm.currentAction = STR('cola');
		return true;
	}else{
		req.session.vm.currentAction = STR('sold_out');
		return false;
	}
}

function handleCase(req, res, paramMap){
	switch(req.session.vm.currentAction) {
		case STR('cola'):
			paramMap.displayVal = 'THANK YOU';
			paramMap.reload = true;
			break;
		case STR('chips'):
			paramMap.displayVal = 'THANK YOU';
			paramMap.reload = true;
			break;
		case STR('candy'):
			paramMap.displayVal = 'THANK YOU';
			paramMap.reload = true;
			break;
		case STR('NOT_ENOUGH_FUNDS_COLA'):
			paramMap.displayVal = 'PRICE: $' + STR('COLA_PRICE');
			paramMap.reload = true;
			break;
		case STR('NOT_ENOUGH_FUNDS_CHIPS'):
			paramMap.displayVal = 'PRICE: $' + STR('CHIPS_PRICE');
			paramMap.reload = true;
			break;
		case STR('NOT_ENOUGH_FUNDS_CANDY'):
			paramMap.displayVal = 'PRICE: $' + STR('CANDY_PRICE');
			paramMap.reload = true;
			break;
		case STR('sold_out'):
			paramMap.displayVal = 'SOLD OUT';
			paramMap.reload = true;
			break;
		default:
			paramMap.displayVal = ((req.session.vm.current_CoinValue == 0) ? 'INSERT COIN': "$" + req.session.vm.current_CoinValue);
	}
}

function STR(strVal){
	try{
		var library = {
			'penny': 'penny',
			'nickel': 'nickel',
			'dime': 'dime',
			'quarter': 'quarter',
			'init': '',
			'cola': 'COLA',
			'NOT_ENOUGH_FUNDS_COLA': 'NO MONEY FOR DRINK',
			'chips': 'CHIPS',
			'NOT_ENOUGH_FUNDS_CHIPS': 'NO MONEY FOR CHIPS',
			'candy': 'CANDY',
			'NOT_ENOUGH_FUNDS_CANDY': 'NO MONEY FOR CANDY',
			'COLA_PRICE': '1.00',
			'CHIPS_PRICE': '0.50',
			'CANDY_PRICE': '0.65',
			'sold_out': 'SOLD_OUT'
		}
		return library[strVal];
	}
	catch(e){
		library = null;
	}finally{
		library = null;
	}
}
