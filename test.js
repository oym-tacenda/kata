var expect  = require('./res/chai').expect;
var request = require('./res/request');
const jsdom = require("./res/jsdom");
const { JSDOM } = jsdom;

describe("Accept Coins Test", function() {
  describe("Checks to see if there is a vending machine", function() {
      var url = "http://localhost:3000";
      it("returns status 200", function(done) {
        request(url, function(error, response, body) {
          expect(response.statusCode).to.equal(200);
          done();
        });
      });
  });

  describe("Validate there are no coins", function() {
      var url = "http://localhost:3000";
      it("Yup, INSERT COIN is displayed", function(done) {
        request(url, function(error, response, body) {
          var dom = new JSDOM(body)
          expect(dom.window.document.getElementById('coinText').textContent).to.equal('INSERT COIN');
          done();
        });
      });
  });

  describe("Do not accept penny", function() {
      var url = "http://localhost:3000/coins";
      it("Penny rejected", function(done) {
        request.post({url, form: {weight: 2.5, size:19.05}}, function(error, response, body) {
          expect(body).to.equal('{"response":"E"}');
              done();
        });
      });
  });

  describe("Accept nickel", function() {
      var url = "http://localhost:3000/coins";
      it("Nickel accepted", function(done) {
        request.post({url, form: {weight:  5, size:21.21}}, function(error, response, body) {
          expect(body).to.equal('{"response":"S"}');
          done();
        });
      });
  });

  describe("Accept dime", function() {
      var url = "http://localhost:3000/coins";
      it("Dime accepted", function(done) {
        request.post({url, form: {weight: 2.268, size:17.91}}, function(error, response, body) {
          expect(body).to.equal('{"response":"S"}');
          done();
        });
      });
  });

  describe("Accept quarter", function() {
      var url = "http://localhost:3000/coins";
      it("Quarter accepted", function(done) {
        request.post({url, form: {weight: 5.67, size:24.26}}, function(error, response, body) {
          expect(body).to.equal('{"response":"S"}');
          done();
        });
      });
  });
});

describe("Select Product Test", function() {
  describe("Validate there is cola", function() {
      var url = "http://localhost:3000/cola";
      it("Yup, I see the sign", function(done) {
        request(url, function(error, response, body) {
          expect(response.statusCode).to.equal(200);
          done();
        });
      });
  });

  describe("Cola's price is $" + STR('COLA_PRICE'), function() {
      var url = "http://localhost:3000/cola";
      it("Yup, cola equals $" + STR('COLA_PRICE'), function(done) {
        request(url, function(error, response, body) {
          var dom = new JSDOM(body)
          expect(dom.window.document.getElementById('price').textContent).to.equal('$' + STR('COLA_PRICE'));
          done();
        });
      });
  });

  describe("Validate there are chips", function() {
      var url = "http://localhost:3000/chips";
      it("Yup, I see the sign", function(done) {
        request(url, function(error, response, body) {
          expect(response.statusCode).to.equal(200);
          done();
        });
      });
  });

  describe("Chips' price is $" + STR('CHIPS_PRICE'), function() {
      var url = "http://localhost:3000/chips";
      it("Yup, chips equals $" + STR('CHIPS_PRICE'), function(done) {
        request(url, function(error, response, body) {
          var dom = new JSDOM(body)
          expect(dom.window.document.getElementById('price').textContent).to.equal('$' + STR('CHIPS_PRICE'));
          done();
        });
      });
  });

  describe("Validate there is candy", function() {
      var url = "http://localhost:3000/candy";
      it("Yup, I see the sign", function(done) {
        request(url, function(error, response, body) {
          expect(response.statusCode).to.equal(200);
          done();
        });
      });
  });

  describe("Candy's price is $" + STR('CANDY_PRICE'), function() {
      var url = "http://localhost:3000/candy";
      it("Yup, candy equals $" + STR('CANDY_PRICE'), function(done) {
        request(url, function(error, response, body) {
          var dom = new JSDOM(body)
          expect(dom.window.document.getElementById('price').textContent).to.equal('$' + STR('CANDY_PRICE'));
          done();
        });
      });
  });
});

describe("Make Change & Return Coins", function() {
  describe("Validate there are the correct number of coins", function() {
      var url = "http://localhost:3000";
      it("Yup, all the initial money is there", function(done) {
        request(url, function(error, response, body) {
          var dom = new JSDOM(body)
          expect(dom.window.document.getElementById('pennylabel').textContent).to.equal('1');
          expect(dom.window.document.getElementById('nickellabel').textContent).to.equal('10');
          expect(dom.window.document.getElementById('dimelabel').textContent).to.equal('10');
          expect(dom.window.document.getElementById('quarterlabel').textContent).to.equal('10');
          done();
        });
      });
  });

  describe("Lets enter all of our quarters and buy a soda", function() {
      var url = "http://localhost:3000/test/1";
      it("Yup, I got correct change", function(done) {
        request(url, function(error, response, body) {
          var dom = new JSDOM(body)
          expect(dom.window.document.getElementById('pennylabel').textContent).to.equal('1');
          expect(dom.window.document.getElementById('nickellabel').textContent).to.equal('10');
          expect(dom.window.document.getElementById('dimelabel').textContent).to.equal('10');
          expect(dom.window.document.getElementById('quarterlabel').textContent).to.equal('6');
          done();
        });
      });
  });

  describe("Lets enter 4 of our quarters and all of our dimes & buy candy", function() {
      var url = "http://localhost:3000/test/2";
      it("Yup, I got correct change", function(done) {
        request(url, function(error, response, body) {
          var dom = new JSDOM(body)
          expect(dom.window.document.getElementById('pennylabel').textContent).to.equal('1');
          expect(dom.window.document.getElementById('nickellabel').textContent).to.equal('1');
          expect(dom.window.document.getElementById('dimelabel').textContent).to.equal('8');
          expect(dom.window.document.getElementById('quarterlabel').textContent).to.equal('10');
          done();
        });
      });
  });

  describe("Lets enter all of our money and hit the coin return", function() {
      var url = "http://localhost:3000/test/3";
      it("Awesome! I have all my money back", function(done) {
        request(url, function(error, response, body) {
          var dom = new JSDOM(body);
          //This way we can correctly check regardless of how many coins are in the machine
          var total = (dom.window.document.getElementById('nickellabel').textContent * 5)
          + (dom.window.document.getElementById('dimelabel').textContent * 10)
          + (dom.window.document.getElementById('quarterlabel').textContent * 25);
          expect(total).to.equal(400);
          done();
        });
      });
  });

  describe("Lets buy a cola, and then try to buy another!", function() {
      var url = "http://localhost:3000/test/4";
      it("Yup, the cola is sold out", function(done) {
        request(url, function(error, response, body) {
          var dom = new JSDOM(body);
          expect(dom.window.document.getElementById('coinText').textContent).to.equal('SOLD OUT');
          done();
        });
      });
  });
});

function STR(strVal){
	try{
		var library = {
			'COLA_PRICE': '1.00',
			'CHIPS_PRICE': '0.50',
			'CANDY_PRICE': '0.65'
		}
		return library[strVal];
	}
	catch(e){
		library = null;
	}finally{
		library = null;
	}
}